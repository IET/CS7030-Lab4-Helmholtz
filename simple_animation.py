import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation

#Medium parameters
density = 1
pressure = 0
mass_velocity = 0

#d/dt(density') + nam(density*mass_velocity) = 0
#(density*(d/dt(mass_velocity')) + nam(density') = 0

#Helmholtz for monochromatic wave of frequency v
#(nambla^2 + k^2)U(p) = 0		where k = 2pi(v/c) = 2pi/lambda

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 2), ylim=(-2, 2))
line, = ax.plot([], [], lw=2)

# initialization function: plot the background of each frame
def init():
    line.set_data([], [])
    return line,


# animation function.  This is called sequentially
def animate(i):
    x = np.linspace(0, 2, 1000)
    y = np.sin(2 * np.pi * (x - 0.01 * i))
    line.set_data(x, y)
    return line,

# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=200, interval=20, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html
print ("Saving animation as MPEG4")
anim.save('basic_animation.mp4', writer='ffmpeg', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()