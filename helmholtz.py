import numpy as np
import scipy
from scipy import signal
import pylab
from scipy.io import wavfile
import matplotlib.pyplot as plt
from matplotlib import animation

frequency = 1/50 #seconds per cycle
amplitude = 1
phase     = 45 #degrees

# First set up the figure, the axis, and the plot element we want to animate
fig = plt.figure()
ax = plt.axes(xlim=(0, 10), ylim=(-2, 2))
line, = ax.plot([], [], lw=2)

# initialization function: plot the background of each frame
def init():
	line.set_data([], [])
	return line,

# animation function.  This is called sequentially
def animate(i):
	p = np.linspace(0, 360, num=10000)
	y = u(p,i)
	line.set_data(p, y)
	return line,

def phi(p):
	global phase
	return (phase + p) % 360

def U(p):
	global amplitude
	return amplitude * np.exp(1j * phi(p))

def u(p,t):
	global frequency
	return U(p) * np.exp(-1j * 2 * np.pi * frequency * t)


# call the animator.  blit=True means only re-draw the parts that have changed.
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=1000, interval=50, blit=True)

# save the animation as an mp4.  This requires ffmpeg or mencoder to be
# installed.  The extra_args ensure that the x264 codec is used, so that
# the video can be embedded in html5.  You may need to adjust this for
# your system: for more information, see
# http://matplotlib.sourceforge.net/api/animation_api.html

#print ("Saving animation as MPEG4")
#anim.save('sinusoid.mp4', writer='ffmpeg', fps=30, extra_args=['-vcodec', 'libx264'])

plt.show()